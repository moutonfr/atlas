Slingshot.fileRestrictions( "uploadToAmazonS3", {
  allowedFileTypes: [ "image/png", "image/jpeg", "image/gif", "application/pdf", "application/x-pdf", "application/zip" ],
  maxSize: 20 * 1024 * 1024
});

Slingshot.createDirective( "uploadToAmazonS3", Slingshot.S3Storage, {
  bucket: "materialsatlasprj",
  acl: "public-read",
  authorize: function () {
//    let userFileCount = Files.find( { "userId": this.userId } ).count();
//    return userFileCount < 3 ? true : false;
      return true;
  },
  key: function ( file ) {
//    var user = Meteor.users.findOne( this.userId );
//    return user.emails[0].address + "/" + file.name;
    return file.name;
  }
});
