Meteor.publish('wafer', function(waferId) {
	check(waferId, String);
	return Wafers.find({ _id: waferId });
});
