Meteor.publish('xrd', function(xrdId) {
	check(xrdId, String);
	return XRD.find({ _id: xrdId });
});
