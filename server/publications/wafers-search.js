Meteor.publish('wafersSearchByComposition', function(composition, and, showHidden) {
	check(composition, [String]);
    check(and, Boolean);
    check(showHidden, Boolean)

    var filter = _.map(composition, function (el) { return { 'properties.composition': { $in: [el] } } });

    var filterParam = composition.length == 0 ?
        { 'properties.composition': { $in: [] } } :
        and == true ? { $and: filter } : { $or: filter };

    // only currator and admins can see hidden element and have access to 'show hidden' feature
    if (!Roles.userIsInRole(this.userId, ['curator', 'administrator']) || !showHidden) {
        filterParam["properties.isHidden"] = { $ne: true };
    }
    let waf = Wafers.find(filterParam,
		{
			fields: { name: 1, properties: 1 },
			sort: { name: 1 }
		});

    return waf;
});
