Meteor.publish('searchByNames', function(names) {
	check(names, [String]);
	return [
		Wafers.find({name: {$in: names} }, { fields: { data: 0 } }),
		XRD.find({name: {$in: names} }, { fields: { name: 1 } })
	];
});
