Router.configure({
	// layoutTemplate: 'appBody',
	layoutTemplate: 'adminLayout',
});
Router.route('/', {
	name: 'elementTable',
	template: 'elementTable',
	loadingTemplate: 'loading',
	layoutTemplate: 'adminLayout',
	waitOn: function() {
		var ret = Meteor.subscribe('elements');
		return ret;
	}
});
Router.route('/upload', function() {
    this.render('upload')
}, {
    name: 'upload'
});
Router.route('/users', {
	name: 'users',
	template: 'users',
	loadingTemplate: 'loading',
	layoutTemplate: 'adminLayout',
	waitOn: function() {
		var ret = Meteor.subscribe('users');
		return ret;
	}
});
Router.route('/loading', {
    template: 'loading',
	layoutTemplate: 'adminLayout'
});
Router.route('/w/:_id', {
    name: 'wafer',
    template: 'wafer',
    loadingTemplate: 'loading',
    data: function() {
        return Wafers.findOne({
            _id: this.params._id
        });
    },
    waitOn: function() {
        return Meteor.subscribe('wafer', this.params._id);
    }
});

function search(query, response) {
	var wafers = Wafers.find({name: {$regex: query}},
		{ fields: {
		_id: true,
		name: true
	}}).fetch();

	if (wafers == []) {
		response.writeHead(204);
		response.end();
	}
	else {
		response.setHeader('Content-Type', 'application/json');
		response.end(JSON.stringify(wafers));
	}
}

Router.route('/api/edx-list', function() {
	search('.*', this.response);
}, {where:'server'});
Router.route('/api/edx-list/:search', function() {
	search(this.params.search, this.response)
}, {where:'server'});
Router.route('/api/edx/:_id', function() {
	var self = this;
	var wafer = Wafers.findOne({_id:this.params._id},
		{ fields: {
			_id: true,
			name: true,
			properties: true,
			data: true,
			XRD: true,
			EDX: true
		}});

	if (wafer == undefined) {
		this.response.writeHead(204);
		this.response.end();
	}
	else {
		this.response.setHeader('Content-Type', 'application/json');
		this.response.end(JSON.stringify(wafer));
	}
}, {where:'server'});
Router.route('/api/xrd/:_id', function() {
	var self = this;
	var xrd = XRD.findOne({_id:this.params._id},
		{ fields: {
		_id: true,
		data: true
	}});

	if (xrd == undefined) {
		this.response.writeHead(204);
		this.response.end();
	}
	else {
		this.response.setHeader('Content-Type', 'application/json');
		this.response.end(JSON.stringify(xrd));
	}
}, {where:'server'});

Router.route("/a/:_id", {
	name:"waferold",
	template:"admin",
	layoutTemplate: 'adminLayout',
	loadingTemplate: 'loading',
    data: function() {
        return Wafers.findOne({
            _id: this.params._id
        });
    },
    waitOn: function() {
        return Meteor.subscribe('wafer', this.params._id);
    }
});

AccountsTemplates.configureRoute('signIn', {
    name: 'signin',
    path: '/signin',
});
