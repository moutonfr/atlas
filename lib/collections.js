Elements = new Mongo.Collection("elements");
Wafers = new Mongo.Collection("wafers");
XRD = new Mongo.Collection("xrd");


if (Meteor.isServer) {
    Meteor.methods({
        insertXRD: function(key, data) {
//            check(data)
            XRD.remove({ name: key });
            XRD.batchInsert(data);

            return key;
        },
        saveMetadata: function(waferId, selectedOption, metadata) {
        	check(waferId, String);
        	check(metadata, String);

            var w = Wafers.findOne({_id:waferId});
            w[selectedOption].metadata = metadata

            Wafers.update({ _id: w._id }, w);

            return Wafers.findOne({_id:waferId});
        },
        setHidden: function(waferId, isHidden) {
            Wafers.update({ _id: waferId }, { $set: { "properties.isHidden": isHidden } });

            return isHidden;
        },
        insertUpdateWafer: function(key, data) {
            check(key, String);

            var w = Wafers.findOne({name: key});

            if (w === undefined) {
                var id = Wafers.insert({name: key});
                w = Wafers.findOne({_id: id});
            }
            w.data = data.data;
            w.properties = data.properties;
            Wafers.update({ _id: w._id }, w);
            return key;
        },
        updateMetadataSummary: function(key,type,data)
        {
            // type: metadata or summary
            check(key, String);
            check(type, String);

            var w = Wafers.findOne({name: key});

            if (w === undefined) {
                var id = Wafers.insert({name: key});
                w = Wafers.findOne({_id: id});
            }
            for (var opt in data) {
                if (w[opt] === undefined) {
                    w[opt] = {};
                }
                w[opt][type] = data[opt];
            }
            Wafers.update({ _id: w._id }, w);
            return key;
        },
        updateFeatures: function (key, data) {
            check(key, String);
            var w = Wafers.findOne({name: key});

            if (w === undefined) {
                var id = Wafers.insert({name: key});
                w = Wafers.findOne({_id: id});
            }

            for (var opt in data) {
                if (w[opt] === undefined) {
                    w[opt] = {};
                }
                if (w[opt].features === undefined) {
                    w[opt].features = [];
                }

                for (var i = 0, img; img = data[opt][i]; i++) {
                    var feature = w[opt].features.filter(function(f) {
                        return (f.x === img.x) && (f.y === img.y);
                    });
                    if (feature.length > 0) {
                        for (var f in img) {
                            feature[0][f] = img[f];
                        }
                    }
                    else {
                        w[opt].features.push(img);
                    }
                }
            }

            Wafers.update({ _id: w._id }, w);
            return key;
        },
        linkXRDEDX(key) {
            check( key, String );
            var wafer = Wafers.findOne({ name: key });

            if (wafer.data) {
                var xrds = XRD.find(
                    {name: key},
                    { fields: {data: 0} }).fetch();

                for (var i = 0, w; w = wafer.data[i]; i++) {
                    var xrd = xrds.filter(function (x) {
                        return (x.x === w.x) && (x.y === w.y);
                    });

                    if (xrd.length > 0) {
                        w.xrdId = xrd[0]._id;
                    } else {
                        delete(w.xrdId);
                    }
                }
            }
            Wafers.update({ _id: wafer._id }, wafer);
            return key;
        },
        setRoleOnUser(options) {
            check(options, {
                user: String,
                role: String
            });

            try {
                console.log(options);
                Roles.setUserRoles(options.user, [options.role]);

            }
            catch (exception) {
                return exception;
            }
        }

    });
}
