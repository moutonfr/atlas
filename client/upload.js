Template.upload.events({
    'change #zip': function (event, template) {
        // JSZip
        $('div[name="please-wait"').removeClass('hidden');
        $result = $('#result');
        $result.text('');
        var files = event.target.files;
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            reader.onload = (function(file, elementsFound) {
                return function(e) {
                    var ret = parseZip(e, file);
                    elementsFound.set(ret);
                };
            })(f, template.elementsFound);

            reader.readAsArrayBuffer(f);

        }
    },
    'submit form': function (e) {
        e.preventDefault();

        // $("#wait").show();

        $(".glyphicon").detach();

        $("#message")
            .attr("class", "label label-info")
            .text("Upload in progress, please wait")
            .show();

        $('button[type=submit]').prop('disabled', true);

        var elementsFound = Template.instance().elementsFound.get();

        setTimeout(function(elementsFound){

            for (var key in elementsFound) {
                for (var type in elementsFound[key]) {

                    var check = $("[data-key='" + key + "'] div[name='" + type + "']");

                    if ($("input:checked", check).length > 0) {

                        // $("<div>Uploading " + type + " for " + key +
                        //   " <span name='"+key+type+"' class='glyphicon glyphicon-upload'></span></div>")
                        // .appendTo("#wait > div");

                        $("<span data-name='" + key + type +
                            "' class='glyphicon glyphicon-upload'></span>")
                        .appendTo(check);


                        switch(type) {
                            case "edx":
                                let sampleId = $('#sampleId').val(), howToCite = $('#howToCite').val();
                                if (sampleId != '') elementsFound[key][type].properties.sampleId = sampleId;
                                if (howToCite != '') elementsFound[key][type].properties.cite = howToCite;
                                Meteor.call("insertUpdateWafer",
                                           key, elementsFound[key][type],
                                           function(error, result){
                                    Meteor.call("linkXRDEDX", result, function(error, result) {
                                        updateStatus(result + "edx");
                                    })
                                });
                                break;
                            case "xrd":
                                Meteor.call("insertXRD",
                                           key, elementsFound[key][type],
                                           function(error, result){
                                    Meteor.call("linkXRDEDX", result, function(error, result) {
                                        updateStatus(result + "xrd");
                                    })
                                });
                                break;
                            case "metadata":
                                Meteor.call("updateMetadataSummary",
                                    key, "metadata", elementsFound[key][type],
                                    function (error, result) {2
                                        updateStatus(result + "metadata");
                                    });
                                break;
                            case "summary":
                                var st = new ReactiveVar(
                                  {
                                    initkey: key,
                                    inittype: type
                                  },
                                  function ch(oldValue, newValue) {
                                    var type = newValue.inittype, key = newValue.initkey;
                                    console.log(newValue[key]);
                                    var allComplete = true;
                                    for (var opt in elementsFound[key][type]) {
                                        allComplete = allComplete && (newValue[key][opt] != undefined);
                                    }
                                    if (allComplete) {
                                        Meteor.call("updateMetadataSummary",
                                            key, "summary", newValue[key],
                                            function(error, result) {
                                                updateStatus(result + "summary");
                                        })
                                        console.log(allComplete);
                                    }
                                })
                                for (var opt in elementsFound[key][type]) {
                                    console.log(elementsFound[key][type][opt]);
                                    Modules.client.uploadToAmazonS3(
                                        elementsFound[key][type][opt],
                                        (function(key,opt,st) {
                                            return function (error, downloadURL) {
                                                if (error) {
                                                    console.log(error);
                                                }
                                                else {
                                                    console.log(downloadURL);
                                                    var newSt = st.get();
                                                    if (newSt[key] === undefined) {
                                                        newSt[key] = {};
                                                    }
                                                    newSt[key][opt] = downloadURL;
                                                    st.set(newSt);
                                                }
                                            }
                                        })(key,opt,st)
                                    );
                                }
                                break;
                            case "images":
                                var st = new ReactiveVar(
                                  {
                                    total: elementsFound[key][type].length,
                                    actual: 0,
                                    currentElements: elementsFound[key][type]
                                  },
                                  function ch(oldValue, newValue) {
                                    console.log(newValue);
                                    if (newValue.total === newValue.actual) {
                                        // all images uploaded, ready to update db info
                                        var data = {};
                                        for (var i = 0, d; d = newValue.currentElements[i]; i++) {
                                            if (data[d.option] === undefined) data[d.option] = [];
                                            data[d.option].push({
                                                x: d.x,
                                                y: d.y,
                                                image: d.name
                                            });
                                        }
                                        console.log(data);

                                        Meteor.call("updateFeatures",
                                            key, data,
                                            function(error, result) {
                                                updateStatus(result + "images");
                                        });
                                    }
                                })
                                for (var i=0; i < elementsFound[key][type].length; i++) {
                                    Modules.client.uploadToAmazonS3(
                                        elementsFound[key][type][i].file,
                                        (function(element,st) {
                                            return function (error, downloadURL) {
                                                if (error) {
                                                    console.log(error);

                                                }
                                                else {
                                                    console.log(downloadURL);
                                                    element.name = downloadURL;
                                                    var newSt = st.get();
                                                    newSt.actual++;
                                                    st.set(newSt);
                                                }
                                            }
                                        })(elementsFound[key][type][i],st)
                                    );
                                }
                            break;
                            case "textdata":
                                Meteor.call("updateFeatures",
                                    key, elementsFound[key][type],
                                    function(error, result) {
                                        updateStatus(result + "textdata");
                                });
                            break;
                        }
                    }
                    else {
                        $("<span class='glyphicon glyphicon-ban-circle'></span>")
                        .appendTo(check);

                    }

                }
            }
        }, 500  , elementsFound);

        return;

    },
    'click #help': function(e) {
        $("#help-text").toggle();
    }

});


Template.upload.helpers({
    composition: function() {
        var elementsFound = Template.instance().elementsFound.get();

        var ret = [];
        var index = 0;
        for (var key in elementsFound) {
            var elem = {name: key, index: index, check: []};

            if (elementsFound[key].edx) {
                elem.check.push({
                    name: "edx",
                    inFile: true,
                    inDB: Wafers.find({
                        name: key,
                        'properties.composition': { $exists: true, $ne: [] }
                    }, {reactive: false}).count() > 0
                })
            }
            if (elementsFound[key].xrd) {
                elem.check.push({
                    name: "xrd",
                    inFile: true,
                    inDB: XRD.find({
                        name: key
                    }, { reactive: false }).count() > 0
                })
            }
            if (elementsFound[key].metadata) {
                elem.check.push({
                    name: "metadata",
                    inFile: true,
                    inDB: Wafers.find({
                        name: key,
                        'EDX.metadata': { $exists: true, $ne: [] }
                    }, {reactive: false}).count() > 0
                });
                // Search for Sample ID
                if (elementsFound[key].metadata.EDX){
                    let s = /^sample id: (.*)$/mi;
                    let m = elementsFound[key].metadata.EDX.match(s);
                    if (m) elem.sampleId = m[1];
                }
            }
            if (elementsFound[key].summary) {
                elem.check.push({
                    name: "summary",
                    inFile: true,
                    inDB: Wafers.find({
                        name: key,
                        'EDX.summary': { $exists: true, $ne: [] }
                    }, {reactive: false}).count() > 0
                })
            }
            if (elementsFound[key].images) {
                elem.check.push({
                    name: 'images',
                    inFile: true,
                    inDB: false
                })
            }
            if (elementsFound[key].textdata) {
                elem.check.push({
                    name: 'textdata',
                    inFile: true,
                    inDB: false
                })
            }


            ret.push(elem);
            index++;

        }
        $('div[name="please-wait"]').addClass('hidden');
        return ret;
    },
});

Template.registerHelper('_', function () {
    return _;
});
var updateStatus = function (id) {
    $('span[data-name="' + id + '"]')
        .removeClass("glyphicon-upload")
        .addClass("glyphicon-ok-circle");

    // // hide check all complete
    if ($("span.glyphicon-upload").length == 0) {
        $("#message")
            .attr("class", "label label-success")
            .html("Upload successful");

        $('button[type=submit]').prop('disabled', false);


    }
};
Template.upload.onCreated(function() {
    var instance = this;

    instance.elementsFound = new ReactiveVar([]);
    instance.uploadStatus = new ReactiveVar([]);

    instance.autorun(function() {
        var elementsFound = instance.elementsFound.get();
        var names = Object.keys(elementsFound);

        instance.subscribe('searchByNames', names);
    });
});

function parseZip(e, file) {
    var $fileContent = $("<ul>");
    try {
        var dateBefore = new Date();
        // read the content of the file with JSZip
        var zip = new JSZip(e.target.result);
        var dateAfter = new Date();

        // that, or a good ol' for(var entryName in zip.files)
        var elementsFound = [];
        let is2018 = $('input[name="2018specs"]').prop('checked');
        if (is2018) {
            let dsName;
            if (zip.files != {}) {
                dsName = _.chain(zip.files)
                    .keys()
                    .map(function(file) { return file.split('/')})
                    .flatten()
                    .first()
                    .value(); // get dataset name
            }
            else {
                throw "No files in zip.";
            }

            let element = {};
            let files = {};
            _.each(zip.files, function(zip, key) {
                files[key.toLowerCase()] = zip;
            });

            let curFile = (dsName + '/EDS metadata.txt').toLowerCase();
            if (_.has(files, curFile)) {
                element.metadata = { };
                element.metadata["EDX"]= files[curFile].asText();
            }
            else { throw curFile + ' not found'; }

            curFile = (dsName + '/fabrication metadata.txt').toLowerCase();
            if (_.has(files, curFile)) {
                element.metadata["fabrication"]= files[curFile].asText();
            }
            else { throw curFile + ' not found'; }

            curFile = (dsName + '/XRD metadata.txt').toLowerCase();
            if (_.has(files, curFile)) {
                element.metadata["XRD"]= files[curFile].asText();
            }
            else { throw curFile + ' not found'; }

            curFile = (dsName + '/SummaryPDF.pdf').toLowerCase();
            if (_.has(files, curFile)) {
                element.summary = {};
                let theFile = new File([files[curFile].asArrayBuffer()], curFile, { type: "application/pdf" });
                element.summary["EDX"]= theFile;
            }
            else { throw curFile + ' not found'; }

            curFile = (dsName + '/EDX label.txt').toLowerCase();
            if (_.has(files, curFile)) {
                element.edx = {
                    name: dsName
                };
                let lines = files[curFile].asText().split('\n');
                let regEx = /(.*): '(.*)'/;
                let labels = {};
                for (let line = 0; line < lines.length; line++) {
                    if (lines[line] != '') {
                        let myArray = regEx.exec(lines[line]);
                        labels[myArray[1].replace('.', '')] = myArray[2];
                    }
                }
                element.edx.properties = { labels: labels};
            }
            else { throw curFile + ' not found'; }

            curFile = (dsName + '/EDX data.csv').toLowerCase();
            if (_.has(files, curFile)) {
                let csv = Papa.parse(files[curFile].asText(), {
                    delimiter: ',', quoteChar: '"', header: true, skipEmptyLines: true,
                    complete: function (result) {
                        let data = [], composition = [];
                        for (let i = 0; i < result.data.length; i++) {
                            let coord = {
                                x: result.data[i].X,
                                y: result.data[i].Y,
                                "Gun2": parseFloat(result.data[i]["Gun.2"]),
                                "Gun3": parseFloat(result.data[i]["Gun.3"]),
                                    "Gun4": parseFloat(result.data[i]["Gun.4"])
                                }
                                for (let e = 2; result.meta.fields[e] != 'Gun.2'; e++) {
                                    let theEl = result.meta.fields[e];
                                    coord[theEl] = parseFloat(result.data[i][theEl]);
                                }
                                data.push(coord);
                            }
                            for (let i = 2; result.meta.fields[i] != 'Gun.2'; i++) {
                                composition.push(result.meta.fields[i]);
                            }
                            element.edx.data = data;
                            element.edx.properties.composition = composition;
                        }
                });
            }
            else { throw curFile + ' not found'; }

            let xrdIndex;
            curFile = (dsName + '/XRDIndexToCoords.csv').toLowerCase();
            if (_.has(files, curFile)) {
                Papa.parse(files[curFile].asText(), {
                    header: true,
                    complete: function (result) {

                        xrdIndex = {};
                        _.each(result.data, function (el) {
                            xrdIndex[("000" + el.Index).substr(-3, 3)] = {
                                X: el.X,
                                Y: el.Y
                            };
                        });
                    }
                });
            }
            else { throw curFile + ' not found'; }

            curFile = (dsName + '/XRD Data/').toLowerCase();
            if (_.has(files, curFile)) {
                let xrd = [];
                let siType = /.*_.*_0(\d\d\d)_.*.csv/
                let xrdFiles = _.chain(files)
                    .keys()
                    .filter(function(path) { // get XRD file names
                        return path.indexOf(curFile) != -1 && path.length != curFile.length
                    })
                    .each(function (el) { // and loop through them
                        let indArray = siType.exec(el);
                        let coord = xrdIndex[indArray[1]];
                        Papa.parse(files[el].asText(), {
                            complete: function (result) {
                                let res = _.map(result.data, function(el) {
                                    return _.map(el, function(str) { return parseFloat(str);});
                                })
                                xrd.push({
                                    name: dsName,
                                    x: coord.X,
                                    y: coord.Y,
                                    data: res
                                });
                            }
                        });
                    });
                element.edx.properties['xrd'] = {
                    xlabel: 'q / Angstroms',
                    xmin: 1.5,
                    xmax: 6.6
                }
                element.xrd = xrd;
            }
            else { throw curFile + ' not found'; }

            elementsFound[dsName] = element;
        }
        else {
            $.each(zip.files, function (index, zipEntry) {
                if (zipEntry.name.indexOf('__MACOSX') != -1 ||
                    zipEntry.name.indexOf('.DS_Store') != -1) return;

                let path = zipEntry.name.split('/');
                let fileName = path.pop();
                let extension = fileName.replace(/^.*(\.[a-zA-Z]{3,4})$/, '$1');
                let fileType = path.pop();

                let material = path.pop();
                if (fileType == 'EDX' &&
                    fileName.indexOf(".txt") != -1 &&
                    material != "" &&
                    zipEntry.name[0] != "_") {

                    var edx = {
                        name: material,
                        data: []
                    };

                    // the content is here
                    // $fileContent.append($("<li>", {
                    //     text: zipEntry.asText()
                    // }));
                    var csv = Papa.parse(zipEntry.asText(), {
                        delimiter: "\t",
                        complete: function (results, file) {
                            var plotData = [];
                            var needHeader = true;
                            $.each(results.data, function (index, data) {
                                if (data.length > 1) {
                                    if (needHeader) { // parse header
                                        var composition = [];
                                        for (var i = 2; i < data.length; i++) {
                                            var element = data[i]
                                                .replace(/(.*) .*$/, "$1");
                                            if (data[i] != "")
                                                composition.push(element);
                                        }
                                        edx.properties = { composition: composition };
                                        needHeader = false;
                                    } else {
                                        var d = {};
                                        for (var i = 0; i < data.length; i++) {
                                            if (i == 0) {
                                                // Yanhui's formula
                                                d.x = parseFloat(data[i]);
                                            }
                                            else if (i == 1) {
                                                // Yanhui's formula
                                                d.y = parseFloat(data[i]);
                                            }
                                            else if (edx.properties.composition[i - 2] != undefined)
                                                d[edx.properties.composition[i - 2]] = parseFloat(data[i]);
                                        }
                                        edx.data.push(d);
                                    }
                                }
                            });
                        }
                    });

                    if (elementsFound[material] == undefined) {
                        elementsFound[material] = {};
                    }

                    elementsFound[material].edx = edx;
                    // Wafers.insert(edx);
                }

                if (fileType == 'XRD' &&
                    fileName.indexOf('.ras') != -1 &&
                    material != "") {

                    //                var coordX = parseInt(fileName.replace(/.*\_([0-9]{3})[0-9]{3}\.ras/, '$1'));
                    //                var coordY = parseInt(fileName.replace(/.*\_[0-9]{3}([0-9]{3})\.ras/, '$1'));

                    var xrd = {
                        //                    x: coordX,
                        //                    y: coordY,
                        name: material,
                        data: []
                    };

                    var csv = Papa.parse(zipEntry.asText(), {
                        delimiter: " ",
                        complete: function (results, file) {
                            var plotData = [];
                            var isData = false;
                            $.each(results.data, function (index, data) {
                                if (data[0] === "*MEAS_COND_AXIS_POSITION-6") {
                                    xrd.x = parseInt(data[1]);
                                }
                                if (data[0] === "*MEAS_COND_AXIS_POSITION-7") {
                                    xrd.y = parseInt(data[1]);
                                }
                                if (isData == false) {
                                    /* ignore until */
                                    if (data[0] == "*RAS_INT_START") {
                                        isData = true;
                                    }
                                    return;
                                }
                                if (data.length > 1) {
                                    for (var i = 0; i < data.length; i++) data[i] = parseFloat(data[i]);
                                    xrd.data.push(data);
                                }
                            });
                        }
                    });

                    if (elementsFound[material] == undefined) {
                        elementsFound[material] = {};
                    }

                    if (elementsFound[material].xrd == undefined)
                        elementsFound[material].xrd = [];

                    elementsFound[material].xrd.push(xrd);
                }

                if (fileType == 'Metadata' &&
                    extension === ".txt" &&
                    material != "") {

                    var metadata = {};

                    if (elementsFound[material] == undefined) {
                        elementsFound[material] = {};
                    }
                    else if (elementsFound[material].metadata != undefined) {
                        metadata = elementsFound[material].metadata;
                    }

                    var option = fileName.replace(/^(.*)\.txt$/, '$1');
                    metadata[option] = zipEntry.asText();

                    elementsFound[material].metadata = metadata;
                }

                if (fileType == 'Summary' &&
                    extension === '.pdf' &&
                    material != "") {

                    var summary = {};

                    if (elementsFound[material] == undefined) {
                        elementsFound[material] = {};
                    }
                    else if (elementsFound[material].summary != undefined) {
                        summary = elementsFound[material].summary;
                    }

                    var theFile = new File([zipEntry.asArrayBuffer()], material + "/Summary/" + fileName, {
                        type: "application/pdf"
                    });
                    var option = fileName.replace(/^(.*)\.[a-zA-Z]{3,4}$/, '$1');
                    summary[option] = theFile;

                    elementsFound[material].summary = summary;

                }
                if (fileType == 'Images' &&
                    allowedImageExtensions.indexOf(extension) != -1 &&
                    material != "") {
                    var option = fileName.replace(/^(.*)\_\_[\-\.0-9]+\_\_[\-\.0-9]+\..*$/, '$1');
                    var coordX = parseFloat(fileName.replace(/^.*\_\_([\-\.0-9]+)\_\_[\-\.0-9]+\..*$/, '$1'));
                    var coordY = parseFloat(fileName.replace(/^.*\_\_[\-\.0-9]+\_\_([\-\.0-9]+)\..*$/, '$1'));

                    var images = [];
                    if (elementsFound[material] == undefined) {
                        elementsFound[material] = {};
                    } else if (elementsFound[material].images != undefined) {
                        images = elementsFound[material].images;
                    }

                    var theFile = new File([zipEntry.asArrayBuffer()], material + "/Images/" + fileName, {
                        type: contentType(extension)
                    });

                    var image = {
                        name: fileName,
                        option: option,
                        x: coordX,
                        y: coordY,
                        file: theFile
                    };
                    images.push(image);

                    elementsFound[material].images = images;
                }
                if (fileType == 'Textdata' &&
                    fileName.indexOf('.txt') != -1 &&
                    material != "") {
                    var option = fileName.replace(/^(.*)\.txt$/, '$1');

                    var textdata = []

                    var csv = Papa.parse(zipEntry.asText(), {
                        delimiter: "\t",
                        complete: function (results, file) {
                            var plotData = [];
                            var needHeader = true;
                            var header = [];
                            $.each(results.data, function (index, data) {
                                var theTextdata = {}
                                if (data.length > 1) {
                                    if (needHeader) { // parse header
                                        for (var i = 2; i < data.length; i++) {
                                            var element = data[i]
                                                .replace(/(.*)/, "$1");
                                            if (data[i] != "")
                                                header.push(element);
                                        }
                                        // theTextdata.header = header;
                                        needHeader = false;
                                    } else {
                                        for (var i = 0; i < data.length; i++) {
                                            if (i == 0) {
                                                theTextdata.x = parseFloat(data[i]);
                                            }
                                            else if (i == 1) {
                                                theTextdata.y = parseFloat(data[i]);
                                            }
                                            else if (header[i - 2] != undefined)
                                                theTextdata[header[i - 2]] = parseFloat(data[i]);
                                        }
                                        textdata.push(theTextdata);
                                    }
                                }
                            });
                        }
                    });

                    if (elementsFound[material] == undefined) {
                        elementsFound[material] = {};
                    }
                    if (elementsFound[material].textdata == undefined) {
                        elementsFound[material].textdata = {};
                    }
                    elementsFound[material].textdata[option] = textdata;
                    console.log(textdata);
                }
            });
        }
        // end of the magic !

    } catch (e) {
        let message = e.message === undefined ? e : e.message;
        $fileContent = $("<div>", {
            "class": "alert alert-danger",
            text: "Error reading " + file.name + ": " + message
        });
    }

//    Modules.client.uploadToAmazonS3(file);

    $result.append($fileContent);

    return elementsFound;

};


Template.compo.helpers({
    sampleId: () => {
        return Template.parentData().sampleId || '';
    }
})