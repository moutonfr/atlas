// Template.wafer.created = function() {
//     var self = this;

//     this.autorun(function(a) {
//         var data = Template.currentData(self.view);
//         if (!data) return;

//         Session.set('selectedEl', data.compostion);
//         // plotWafer(data.data, data.composition);
//         // plotTriangle(data.data, data.composition);

//     });

// }

Template.wafer.onCreated(function() {
    $("body").removeClass("layout-top-nav");
});

Template.wafer.onDestroyed(function() {
    $("body").addClass("layout-top-nav");
});


Template.sideMenu.onCreated(function() {
    var exclude = [ "data", "_id", "name", "XRD", "EDX", 'properties' ];
    this.availableOptions = ["EDX","XRD"];
    for (var key in this.data) {
        if (_.indexOf(exclude, key) === -1) {
            this.availableOptions.push(key);
        }
    }

    Session.set('selectedOption', "EDX");
    // Session.set('selectedEl', this.data.composition);
    // this.selectedEl = new ReactiveVar(this.data.composition);
});

// Template.wafer.onRendered(function() {
//     $('#left-nav').affix({
//         offset: {
//             top: $('#topbar').height()
//         }
//     }).width($('#left-nav').width());
// });

Template.sideMenu.helpers({
    availableOptions: function() {
        // return Template.instance().availableOptions;

        return _.map(Template.instance().availableOptions, function(opt) {
            return { opt: opt, selected: opt === Session.get('selectedOption') }
        })
    },
})

Template.sideMenu.events({
    'click #metadata': function() {
        var selectedOption = Session.get('selectedOption');
        var metadata = Template.instance().data[selectedOption].metadata;
        $('#rightbar .panel-heading').html("Metadata for " + selectedOption);
        $('#rightbar .panel-body').html(metadata);
        $('#rightbar').show(100)
    },
    'click #rightbar a': function() {
        $('#rightbar').hide(100);
    },
    'click .sidebar a': function(event) {
        event.preventDefault();

        var option = $(event.target).text();

        Session.set('selectedOption', option);

        d3.selectAll('.wafer-element').remove();
        d3.selectAll('.clicked').classed({'clicked': false });
    }
});


Template.rightSidebar.helpers({
    metadata: function() {
        var selectedOption = Session.get('selectedOption');
        var metaBtn = $("[data-toggle=control-sidebar]").hide();

        if (Template.instance().data[selectedOption] === undefined) {
            return "";
        }
        else {
            if (Template.instance().data[selectedOption].metadata) {
                metaBtn.show();
            }
            else {
                metaBtn.hide();
            }

            var edit = Session.get("edit");

            if (edit === true) {
                return Template.instance().data[selectedOption].metadata;
            }
            else {
                return Template.instance().data[selectedOption].metadata
                .replace(/\n(.*):/g, '<br/><b>$1</b>:')
                .replace(/^(.*):/g, '<b>$1</b>:');
            }
        }
    },
    edit: function() {
        if (Session.get("edit") === true) {
            return true;
        }
        else return false;
    },
});

Template.rightSidebar.events({
    "click .edit-btn": function(event, template){
        var edit = Session.get("edit");

        if (edit) {
            var selectedOption = Session.get('selectedOption');
            var data = Template.instance().data;
            var text = $('.edit-text').val();
            Meteor.call("saveMetadata", data._id, selectedOption, text, function(error, result){
                if(error){
                    console.log("error", error);
                }
            });
        }

        Session.set("edit", edit === undefined ? true : !edit);
    }
});

Template.citation.helpers({
    cite: () => {
        try {
            let data = Template.currentData();
            return data.properties.cite
                .replace('##sampleid##', data.properties.sampleId || '')
                .replace('##url##', 'http://materialsatlasproject.org/w/' + data._id);
        }
        catch (e) {
            return null;
        }
    }
});

Template.wafer.helpers({
    selectedOption: function() {
        return Session.get('selectedOption');
    },
    metadata: function() {
        var selectedOption = Session.get('selectedOption');
        var metaBtn = $("[data-toggle=control-sidebar]").hide();

        if (Template.instance().data[selectedOption] === undefined) {
            return "";
        }
        else {
            if (Template.instance().data[selectedOption].metadata) {
                metaBtn.show();
            }
            else {
                metaBtn.hide();
            }

            return Template.instance().data[selectedOption].metadata
            .replace(/\n(.*):/g, '<br/><b>$1</b>:')
            .replace(/^(.*):/g, '<b>$1</b>:');
        }
    },
    summary: function() {
        var selectedOption = Session.get('selectedOption');
        // if (selectedOption != 'EDX') {
        if (Template.instance().data[selectedOption] === undefined) {
            return ""
        }
        else {
            return Template.instance().data[selectedOption].summary;
        }
        // }
    },
    edit: function() {
        if (Session.get("edit") === true) {
            return true;
        }
        else return false;
    },
});

Template.wafer.events({
    'click [name=show-ternary], change [name=selected-el]': function(event) {
    	var selectedEl = selectedElements();
    	Session.set('selectedEl', selectedEl);
    	// plotTriangle(Template.currentData().data, selectedEl);
    },
    'click .edit-btn': function(event) {
        console.log("btn");
        var edit = Session.get('edit');
        Session.set('edit', edit === undefined ? true : !edit);
    },
    'click [data-toggle="toggle"]': function() {
        $('aside.control-sidebar').toggleClass('control-sidebar-open');
    }
});

Template.toggleClose.events({
    'click [data-toggle="toggle"]': function() {
        $('aside.control-sidebar').toggleClass('control-sidebar-open');
    }
});

Template.plot.onCreated(function() {
    var corners = [];
    var w = 300;
    var h = 260;
    var m = 20;
    var c = [
        [m, h + m],
        [w + m, h + m],
        [(w / 2) + m, m]
    ]
    c.forEach(function(corner, idx) {
        var c1 = idx,
            c2 = idx + 1;
        if (c2 >= c.length) {
            c2 = 0;
        }
        var corner = {
            x1: c[c1][0],
            y1: c[c1][1],
            x2: c[c2][0],
            y2: c[c2][1],
        };
        corners.push(corner);
    });
    this.corners = corners;

    this.coord = function(a, b, c) {
        var sum, pos = [0, 0];
        sum = a + b + c;
        if (sum !== 0) {
            a /= sum;
            b /= sum;
            c /= sum;
            pos[0] = this.corners[0].x1 * a + this.corners[1].x1 * b + this.corners[2].x1 * c;
            pos[1] = this.corners[0].y1 * a + this.corners[1].y1 * b + this.corners[2].y1 * c;
        }
        return pos;
    };
});

Template.plot.helpers({
    viewbox: function() {
        if (this.data) {
            var v = [400, 400, 0, 0];
            $.each(this.data, function(i, d) {
                if (d.x < v[0]) v[0] = d.x - 1;
                if (d.y < v[1]) v[1] = d.y - 1;
                if (d.x > v[2]) v[2] = d.x + 1;
                if (d.y > v[3]) v[3] = d.y + 1;
            });
            return v[0] + " " + v[1] + " " + v[2] + " " + v[3];
        }
        else return '';
    },
    corners: function() {
        return Template.instance().corners;
    },
    ticks: function() {
        // var corners = corners();
        var ticks = [];
        var axisTicks = [0, 20, 40, 60, 80, 100],
            n = axisTicks.length;

        axisTicks.forEach(function(v) {

            var coord1 = Template.instance().coord(v, 0, 100 - v);
            var coord2 = Template.instance().coord(v, 100 - v, 0);
            var coord3 = Template.instance().coord(0, 100 - v, v);
            var coord4 = Template.instance().coord(100 - v, 0, v);
            ticks.push({
                x: coord1[0] - 10,
                y: coord1[1],
                text: v,
                class: "tick-a"
            });
            ticks.push({
                x: coord2[0],
                y: coord2[1] + 13,
                text: 100 - v,
                class: "tick-b"
            });
            ticks.push({
                x: coord3[0] + 10,
                y: coord3[1],
                text: v,
                class: "tick-c"
            });
        });
        return ticks;
    },
    lines: function() {
        var lines = [];
        var axisLines = [0, 20, 40, 60, 80, 100],
            n = axisLines.length;

        axisLines.forEach(function(v) {

            var coord1 = Template.instance().coord(v, 0, 100 - v);
            var coord2 = Template.instance().coord(v, 100 - v, 0);
            var coord3 = Template.instance().coord(0, 100 - v, v);
            var coord4 = Template.instance().coord(100 - v, 0, v);
            if (v !== 0 && v !== 100) {
                lines.push({
                    x1: coord1[0],
                    y1: coord1[1],
                    x2: coord2[0],
                    y2: coord2[1],
                    class: "tick-a"
                });
                lines.push({
                    x1: coord2[0],
                    y1: coord2[1],
                    x2: coord3[0],
                    y2: coord3[1],
                    class: "tick-b"
                });
                lines.push({
                    x1: coord3[0],
                    y1: coord3[1],
                    x2: coord4[0],
                    y2: coord4[1],
                    class: "tick-c"
                });
            }
        });
        return lines;
    },
    elements: function() {
        if (Template.instance().data == null) return [];

        let parent = Template.parentData();

        var selectedEl = Session.get('selectedEl');

        // if labels exists, override element labels
        if (selectedEl === undefined && parent.properties.labels) {
            selectedEl = _.map(parent.properties.labels, function (el) { return el; });
        }
        if (selectedEl == undefined) selectedEl = parent.properties.composition;

        var elements = [];

        for (var i = 0; i < 3; i++) {
            var el = {};
            el.name = selectedEl[i] ? selectedEl[i] : "()";
            switch (i) {
                case 2:
                    el.x = Template.instance().coord(50, 0, 0)[0] + 55;
                    el.y = Template.instance().coord(50, 0, 50)[1] - 15;
                    el.class = "tick-a";
                    el.transform = "rotate(300," + el.x + "," + el.y +")";
                    break;
                case 0:
                    el.x = Template.instance().coord(50, 50, 0)[0];
                    el.y = Template.instance().coord(50, 50, 0)[1] + 26;
                    el.class = "tick-b";
                    break;
                case 1:
                    el.x = Template.instance().coord(0, 50, 50)[0] + 20;
                    el.y = Template.instance().coord(0, 50, 50)[1] - 15;
                    el.class = "tick-c";
                    el.transform = "rotate(60," + el.x + "," + el.y +")";
                    break;
            }
            elements.push(el);
        }

        return elements;
    },
    ternaryData: function() {
    	if (Template.instance().data == null) return [];

    	var data = [];

        var selectedEl = Session.get('selectedEl');
        if (selectedEl == undefined && Template.parentData().properties.labels != undefined)
            selectedEl = _.map(Template.parentData().properties.labels, function(el, key) { return key; });
        if (selectedEl == undefined) selectedEl = Template.parentData().properties.composition;

    	Template.instance().data.data.forEach(function (d, i) {
    		var coord = Template.instance().coord(
    			d[selectedEl[2]],
    			d[selectedEl[0]],
    			d[selectedEl[1]],
    		);
    		var point = {
    		    x: coord[0],
    		    y: coord[1],
    		    r: 2,
                rx: d.x,
                ry: d.y
            };
            _.each(d, function(value, key) {
                if (point[key] === undefined) point[key] = value;
            });
    		data.push(point);
    	});

    	return data;
    },
    coords: function() {
        var coords = Template.instance().data.selectedElement.get();
        console.log(coords);
        return "a";
    },
    xrd: function() {
        // var xrdId = Session.get("xrd");
        // var xrd = XRD.findOne({ _id: xrdId });
        // var maxQ = xrd ? Math.max.apply(Math, xrd.data.map(function(i) { return i[0] })) : null;
        // var maxI = xrd ? Math.max.apply(Math, xrd.data.map(function(i) { return i[1] })) : null;

        // var ret = [];
        // for (var i = 0; i < xrd.data.length; i++)
        //     // if ((xrd.data[i][0]*100) % 1 == 0)
        //         ret.push({i: 400-xrd.data[i][1]*400/maxI, q: xrd.data[i][0]*800/maxQ});
        // return ret;
    },
    optionData: function() {
        var edx = Template.parentData(1);
        var selectedOption = Session.get('selectedOption');

        var ret = [];

        switch (selectedOption) {
            case 'XRD':
                ret = edx.data.filter(function(val) {
                    return val.xrdId != undefined;
                });
                break;
            case 'EDX':
                ret = [];
                break;
            default:
                ret = edx[selectedOption] === undefined ? [] : edx[selectedOption].features;

        }

        if (ret === undefined) return [];

        for (var i = 0, r; r = ret[i]; i++) {
            r.feature = true;
        }

        return _.map(ret, function(d) {
            d.rx = d.x;
            d.ry = d.y;
            return d;
        });
    },
    realData: function(data) {
        return _.map(data, function(d) {
            d.rx = d.x;
            d.ry = d.y;
            return d;
        });
    }

});

// Template.plot.events({
//     'click button': function(e) {
//         // e.preventDefault();
//     alert('click');
//     }
// });

Template.plotCircle.helpers({
    color: function() {
        var selectedEl = Session.get('selectedEl');
        if (selectedEl == undefined && Template.parentData().properties.labels != undefined) selectedEl = _.keys(Template.parentData().properties.labels);
        if (selectedEl == undefined) selectedEl = Template.parentData().properties.composition;
        var ret = [0, 0, 0];
        for (var i = 0; i < selectedEl.length && i < 3; i++) {
            ret[i] = this[selectedEl[i]];
        }
        return "fill: rgb(" +
            Math.floor(ret[2] * 3) + "," +
            Math.floor(ret[1] * 3) + "," +
            Math.floor(ret[0] * 3) + ")";
    },
    class: function() {
    	var selectedEl = Session.get('selectedEl');
        if (selectedEl == undefined) selectedEl = Template.parentData().properties.composition;

        // var class = feature
        // var xrdClass = Template.currentData().xrdId ? "xrd" : "";
        // return "c" + (
        // 	this[selectedEl[0]] + "--" +
        // 	this[selectedEl[1]] + "--" +
        // 	this[selectedEl[2]]).replace(/\./g, "-") +
        //     " " ;//+ class;
        return getClass(this.rx, this.ry);
    },
    radius: function() {
    	return this.r || 1;
    }
});


Template.plotCircle.events({
	'click .xrd, click .feature': function(e, template) {
	    var classes = $(e.target).attr("class").split(" ");

        var theClass = _.find(classes, function(c) { return c[0] === "z"; });

        d3.selectAll("." + theClass).classed({
            "clicked": $.inArray("clicked", classes) == -1,
            "selected": $.inArray("selected", classes) == -1
        });

        if ($.inArray("clicked", classes) > 0) { // deselect

            //d3.select("clicked").remove();
            d3.selectAll("div." + theClass).remove();
            // elems = d3.selectAll("#xrd svg > g");
            //
            // if (elems.size() == 1) {
            //     elems.transition().attr("transform", "scale(1)");
            // }
            // else {
            //     elems.data([[0,0],[960,0],[0,500],[960,500]])
            //         .transition()
            //         .attr("transform", function(d) {
            //             return "scale(.5),translate(" + d[0] + "," + d[1] + ")";
            //         });
            // }
            //

        }
        else {
            // plot xrd
            // if (template.data.xrdId) {

            // if (d3.selectAll("#xrd svg > g").size() == 4) {
            // 	alert("can only display 4 XRD (for now)");
            // 	return;
            // }
            //

            if (template.data.xrdId) {
                var to = setTimeout(function() {
                    $("#wait").show(0);
                }, 200);

                let xrdProperties = Template.parentData().properties.xrd;

                Meteor.subscribe( 'xrd', template.data.xrdId, {
                    onReady: function () {
                        var xrd = XRD.findOne({ _id: template.data.xrdId });

                        $("body").append(Blaze.renderWithData(
                            Template.waferElementTemplate,
                            { data: xrd, xrd: true, xrdProperties: xrdProperties },
                            $("#divXRD").get(0),
                            $("#anchor").get(0)
                        ));

                        clearTimeout(to);
                        $("#wait").hide();
                    }
                })
            }
            else if (template.data.image) {
                $("body").append(Blaze.renderWithData(
                    Template.waferElementTemplate,
                    { data: template.data, xrd: false  },
                    $("#divXRD").get(0),
                    $("#anchor").get(0)
                ));
            }

            // }


        }
	},
	'mouseover circle': function (e, template) {
        var cl = _.find(
            $(e.target).attr("class").split(" "),
            function(c) { return c[0] === "z"; }
        );

        var composition = Template.parentData(1).properties.composition;
        var popup = "<div><p>";
        for (var i=0; i< composition.length; i++) {
            popup += "<b>" + composition[i] + "</b>: "
                    + Math.round(template.data[composition[i]] * 10) / 10 + "% "
        }
        popup += "</p><p><b>x</b>: " + template.data.rx
                + " <b>y</b>: " + template.data.ry + "</p>"
        $("#popup")
            .html(popup)
            .css("left", e.pageX)
            .css("top", e.pageY + 10)
            .show(50);

		d3.selectAll(cl).classed("selected", true);
	},
	'mouseout circle': function (e, template) {
		var cl = "." + $(e.target).attr("class").replace(/ .*/, "");
		d3.selectAll(cl).classed("selected", false);
        $("#popup").hide();

	},
});


Template.waferElementTemplate.helpers({
    title: function() {
        if (this.xrd) {
            return "x = " + this.data.x + ", y = " + this.data.y;
        }
        else {
            return "x = " + this.data.x + ", y = " + this.data.y;
        }
    },
    class: function() {
        return getClass(this.data.x, this.data.y);
    },
    img: function() {
        return this.data.image;
    }
});

Template.waferElementTemplate.events({
    "click button.btn-box-tool:has(.fa-times)": function(event, template){
        // unselect wafer coordinate
        var c = _.find($(event.target).parents(".wafer-element").attr("class").
            split(" "),
            function(c) {
                return c[0] === "z";
            }
        );
        d3.selectAll("." + c).classed({'clicked': false});

        // remove template
        Blaze.remove(template.view);
    },
    "click button.btn-box-tool:has(.fa-plus)": function(event, template){
        var target = $(event.target).parents(".wafer-element").clone();
        target.addClass("el-fullscreen");
        target.removeClass("el-normal");
        target.css("top", $(window).scrollTop() + "px");
        $("body").append(target);

        // hack to remove sticky tooltip
        $(".tooltip", target).remove();

        // remove element as jQuery as can't modify DOM in blaze
        $(".fa-times", target).click(function() {
            target.remove();
        })
    },
});

Template.waferElementTemplate.onRendered(function() {
    var xrd = Template.currentData().data;
    var xrdProperties = Template.currentData().xrdProperties;

    if (xrd._id) {
        var margin = {top: 20, right: 20, bottom: 50, left: 60},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

        let range = xrdProperties === undefined? 
            [0, d3.max(xrd.data, function(d) { return d[0]})] :
            [xrdProperties.xmin, xrdProperties.xmax];

        if (xrdProperties === undefined) {}
        x = d3.scale.linear()
        .domain(range)
        .range([0, width]);

        var y = d3.scale.linear()
        .domain([0, d3.max(xrd.data, function(d) { return d[1]})])
        // .domain([0,3000])
        .range([height, 0]);

        var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .tickSize(-height);

        var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(5)
        .tickSize(-width);

        var line = d3.svg.line()
        .interpolate("monotone")
        .x(function(d) { return x(d[0])})
        .y(function(d) { return y(d[1])})

        var zoom = d3.behavior.zoom()
        .x(x)
        .y(y)
        .scaleExtent([1, 32])
        .on("zoom", zoomed);

        // var templ = $(".elementTemplate > div").clone();

        // $(".content", templ).append("<div id='xrd'><svg></svg></div>");
        // templ.removeBox();

        //var theG;
        var theG = d3.select(this.$("svg").get(0))
        .attr("viewBox",
        "0 0 " + (width + margin.left + margin.right) +
        " " + (height + margin.top + margin.bottom))
        .append("g")
        // .attr("class", classes[0]);
        var svg;


        svg = theG
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        //.call(zoom);

        svg.append("rect")
        // .attr("width", width)
        // .attr("height", height);

        if (xrdProperties != undefined && xrdProperties.xlabel != undefined)
            svg.append('text')
            .text(xrdProperties.xlabel)
            .attr('x', '480')
            .attr('y', '470')
            .attr('text-anchor', 'middle')
            .attr('class', 'tick-text');

        if (xrdProperties != undefined && xrdProperties.ylabel != undefined)
            svg.append('text')
            .text(xrdProperties.ylabel)
            .attr('x', '-40')
            .attr('y', '0')
            .attr('transform', 'rotate(270,-40,0)')
            .attr('text-anchor', 'end')
            .attr('class', 'tick-text');

        svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

        svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

        if (xrdProperties === undefined) {
            svg.append("path")
            .attr("d", line(xrd.data))
            .attr("class", "xrd");
            }
        else
        {
            svg.append("path")
            .attr("d", line(xrd.data.filter(function(data){
                return data[0] >= xrdProperties.xmin && data[0] <= xrdProperties.xmax
            })))
            .attr("class", "xrd");
            }

        svg.append("text")
        .attr("x", width - 6)
        .attr("y", 18)
        .attr("class", "caption")
        .text("(EDX) x: " + template.data.x + ", y: " + template.data.y);

        // $(".box-title", templ).text("(EDX) x: " + template.data.x + ", y: " + template.data.y);

        function zoomed() {
            svg.select("path.xrd").attr("d", line(xrd.data));
            svg.select(".x.axis").call(xAxis);
            svg.select(".y.axis").call(yAxis);
        }
    }

});


function getClass(x, y) {
	return "z" + (x + "--" + y).replace(/\./g, "-");
}

function selectedElements() {
    var selectedEl = [];
    $("[name=selected-el]:checked").each(function() {
        selectedEl.push($(this).val());
    });
    return selectedEl;
}
