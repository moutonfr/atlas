var elWidth = 80;
var gap = 7;

Template.element.helpers({
  x: function() {
    return (this.x - 1) * (elWidth + gap * 2) + gap; //Meteor.settings.public.elementPosition;
  },
  y: function() {
    return (this.y - 1) * (elWidth + gap * 2) + gap; //Meteor.settings.public.elementPosition;
  },
  width: function() {
      return elWidth;
  },
  elText: function() {
      return elWidth / 2;
  },
  elNum: function() {
      return elWidth - 5;
  },
  class: function() {
    var cl = "element";

    return this.symbol ? cl : "";
  },
});

Template.element.events({
  "click g": function(e, t) {
    // $(t.firstNode).addClass("selected").removeClass("element");
    d3.select(t.firstNode).classed("selected", true);

    var composition = [];
    if (Session.get("composition")) composition = Session.get("composition");
    composition.push(this.symbol);
    Session.set("composition", composition);
  },
  "click .selected": function(e, t) {
    // $(t.firstNode).removeClass("selected").addClass("element");
    d3.select(t.firstNode).classed("selected", false);
    var composition = Session.get("composition");
    var newComposition = [];
    for (var i = 0; i < composition.length; i++) {
      if (composition[i] != this.symbol) newComposition.push(composition[i]);
    }
    Session.set("composition", newComposition);
  },
});

Template.elementTable.onDestroyed(function() {
    $("body").removeClass("control-sidebar-open")
});

Template.elementTable.helpers({
  elements: function() {
    var el = Elements.find({}, { $sort: { name: -1 }});
    return el;
  },
  width: function() {
    return (elWidth + gap * 2) * 18;
  },
  height: function() {
    return (elWidth + gap * 2) * 10;
  },
  wafers: function() {
      return Wafers.find({}, { sort: { name: 1 } });
  },
  searchOptions: function() {
      return [
          {
              text: "'And' search",
              class: "search search-and selected",
              bgcolor: "#FFF",
              x: 4.5 * (gap * 2 + elWidth) , y: gap * 3 + elWidth },
          {
              text: "'Or' search",
              class: "search search-or",
              bgcolor: "#FFF",
              x: 6.5 * (gap * 2 + elWidth), y: gap * 3 + elWidth },
          {
              text: "Reset",
              class: "search search-reset",
              bgcolor: "#FFF",
              x: 8.5 * (gap * 2 + elWidth), y: gap * 3 + elWidth },
      ];
  }
});

Template.searchOption.events({
    "click .search-and": function(event, template){
        Session.set("and", true);
    },
    "click .search-or": function(event, template){
        Session.set("and", false);
    },
    "click .search-reset": function(event, template){
        Session.set("and", true);
        Session.set("composition", []);
        d3.selectAll("g.e g.selected").classed("selected", false);
    }
});

Template.elementTable.onCreated(function() {
    var instance = this;

    Session.set("composition", []);
    Session.set("showHidden", true)
    Session.set("and", true);
    d3.select(".search-and").classed("selected", true);
    d3.select(".search-or").classed("selected", false);

    instance.autorun(function() {
        var selectedComposition = Session.get("composition");
        if (selectedComposition.length === 0)
            $("body").removeClass("control-sidebar-open");
        else
            $("body").addClass("control-sidebar-open");

        var and = Session.get("and")
        d3.select(".search-and").classed("selected", and);
        d3.select(".search-or").classed("selected", !and);

        instance.subscribe('wafersSearchByComposition', selectedComposition, and, Session.get("showHidden"));
    })
});

Template.elementTable.events({
  "click #search": function(e, t){
    var composition = [];
    t.$("div.selected").each(function() {
      bl = Blaze.getData(this);
      composition.push(bl.symbol);
    });
    Session.set("composition", composition);
    $(".search-results").show();
    },
});

Template.showHidden.events({
    "click input[name='show-hidden']": function (event) {
        let showHidden = $(event.target).prop('checked');
        Session.set('showHidden', showHidden);
    }
});

Template.searchResults.helpers({
});

Template.searchResults.events({
    "click input[type='checkbox']": function (event, template) {
        let hidden = (this.properties === undefined) || !this.properties.isHidden;
        Meteor.call("setHidden", this._id, hidden, function (error, result) {
            if (error) {
                console.log("error", error);
            }
        });
    }
});
