Template.adminLayout.onRendered(function() {
    $('body').addClass('hold-transition skin-green-light sidebar-mini layout-top-nav');
});


Template.adminLayout.helpers({
    user: function(){
        return Meteor.user().emails[0].address;
    },
});

Template.adminLayout.events({
    "click #logout": function(event, template){
        Meteor.logout();
    }
});
