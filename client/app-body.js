Template.appBody.events({
	'click .menu': function (e) {
		$('.top-menu ul').slideToggle('slow');
	},
	'click a': function (e) {
		if ($('.top-menu ul').is(':visible') && $('.menu').is(':visible')) {
			$('.top-menu ul').slideToggle('fast');
		}
	},
});

