contentType = function contentType(extension) {
    switch (extension) {
        case ".png":
            return "image/png";
            break;
        case ".jpg":
        case ".jpeg":
            return "image/jpeg";
            break;
        case ".gif":
            return "image/gif";
            break;
    }
}

allowedImageExtensions = ['.jpg', '.jpeg', '.png', '.gif'];

Template.registerHelper( 'selected', ( v1, v2 ) => {
  return v1 === v2 ? true : false;
});

Template.registerHelper('currentYear', () => {
    return new Date().getFullYear();
});