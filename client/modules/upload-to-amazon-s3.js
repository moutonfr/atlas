let template;

let upload = ( file, callback ) => {
    const uploader = new Slingshot.Upload("uploadToAmazonS3");
    uploader.send(file, callback);
};

Modules.client.uploadToAmazonS3 = upload;